# github_download_tools

#### 介绍
简单的 github 下载脚本，本质是使用 python 递归换源，可以使用它下载子模块
本脚本只依赖标准 python 库，无需安装其它的库


#### 安装教程

1.  下载并安装 python 
2.  下载并安装 git
3.  下载本脚本
4.  调用指令下载 github库
5.  移动库到自己想要存放的地方

#### 使用说明

1.  打开帮助


```shell
python gdt.py -h
#或者 python gdt.py --help
```

```shell
> python gdt.py -h
made by sky kirto
usage: github_download_tools [-h] [-b BRANCH] [-n NAME] [-s] [-m MIRROR] [-v] git_addr

tool version: v1.1

positional arguments:
  git_addr              github repository source URL

optional arguments:
  -h, --help            show this help message and exit
  -b BRANCH, --branch BRANCH
                        github repository branch
  -n NAME, --name NAME  save folder name
  -s, --submodule       github dowload submodule
  -m MIRROR, --mirror MIRROR
                        github domestic mirror
  -v, --version         tools version
```

2. 下载不带子模块的 github 库

``` shell
python gdt.py https://github.com/espressif/esp-idf.git
```

3. 下载带子模块的 github 库

``` shell
python gdt.py https://github.com/espressif/esp-idf.git -s 
```

4. 下载带子模块的 github 库的指定分支

``` shell
python gdt.py https://github.com/espressif/esp-idf.git -b release/v4.3
```

5. 下载 github 库并重命名

``` shell
python gdt.py https://github.com/espressif/esp-idf.git -n esp_idf
```

6. 下载 github 库并指定镜像

``` shell
python gdt.py https://github.com/espressif/esp-idf.git -m https://github.com.cnpmjs.org/
```

也可以通过修改代码中的 `mirror_num` 指定列表 `__mirror__` 中的镜像源

7. 获取脚本的版本
   
``` shell
python gdt.py -v #或者python gdt.py --version
```

```shell
made by sky kirto
v1.1
```


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 备注

1.  为了使用方便，最好将本脚本加入系统环境变量里，那么后续的python 脚本名，可以直接调用脚本名在任意路径下运行
2.  本仓库适用于linux window macos等各个能装git python的平台
3.  本仓库欢迎各位大佬修改，变得更加好用
