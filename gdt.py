# !/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import re
import shutil
import logging

__version__ = "v1.1"
__mirror__ = [
    "https://ghproxy.com/https://github.com/",
    "https://hub.xn--p8jhe.tw/",
    "https://gitclone.com/github.com/"
]

mirror_num = 0

clone_cmd = ["git", "clone"]
sub_cmd = ["git", "submodule", "update", "--init", "--progress"]

FMTDCIT = {
    'ERROR': "\033[0;31m",
    'INFO': "\033[1;36m",
    'DEBUG': "\033[0;36m",
    'WARN': "\033[1;33m",
    'WARNING': "\033[0;33m",
    'CRITICAL': "\033[1;31m",
    'END': "\033[0m"
}


class Filter(logging.Filter):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)

    def filter(self, record: logging.LogRecord) -> bool:
        record.msg = FMTDCIT[record.levelname] + record.msg + FMTDCIT["END"]
        record.levelname = FMTDCIT[record.levelname] + \
            record.levelname + FMTDCIT["END"]
        return True


def getLogger(name, level=logging.INFO, fmt="\033[1;32m%(asctime)s - %(name)s - %(levelname)s - %(message)s\033[0m", fmt_date="%H:%M:%S"):
    fmter = logging.Formatter(fmt, fmt_date)
    ch = logging.StreamHandler()
    ch.setLevel(level)
    ch.setFormatter(fmter)
    ch.addFilter(filter)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(ch)
    return logger


filter = Filter()
logger = getLogger(__name__, logging.DEBUG)

# 返回版本信息


def get_version():
    """
        This function can get the version information of the script.
    """
    print("made by sky kirto")
    return __version__

# 获得传入参数


def get_args():
    """
        This function can get the parameters passed in when running the script
    """
    # global args
    args = argparse.Namespace()
    parser = argparse.ArgumentParser(
        description=f'tool version: {__version__}', prog="github_download_tools")
    parser.add_argument('git_addr', type=str,
                        help='github repository source URL')
    parser.add_argument('-b', '--branch',  type=str,
                        help='github repository branch')
    parser.add_argument('-n', '--name',  type=str,
                        help='save folder name')
    parser.add_argument('-s', '--submodule',  action='store_true',
                        help='github dowload submodule')
    parser.add_argument('-m', '--mirror', type=str,
                        default=__mirror__[mirror_num], help='github domestic mirror')
    parser.add_argument('-v', '--version', action='version',
                        version=get_version(), help='tools version')
    args = parser.parse_args()
    return args

# 下载主体部分


def download_main(args):
    """
        This function is used to download the main part of the library
    """
    name = ""
    addr = args.git_addr.replace("https://github.com/", args.mirror)
    logger.warning("replace url:%s" % addr)
    clone_cmd.append(addr)
    if args.name is None:
        name = re.findall("([^/]*)\.git", args.git_addr)[0]
    else:
        name = args.name
        clone_cmd.append(name)
    if args.branch != None:
        clone_cmd.extend(["-b", args.branch])
    if not os.path.exists(name):
        os.system(" ".join(clone_cmd))
    else:
        logger.warning("%s is exists" % name)
    return name

# 递归下载子模块


def recursion_sub(args):
    """
        This function is used to download all submodules recursively
    """
    if os.path.exists('.gitmodules'):
        with open('.gitmodules', "r") as f:
            submodules = f.read()
        submodules_copy = submodules
        url_path = re.findall("path = (.*)", submodules)
        submodules = submodules.replace("https://github.com/", args.mirror)
        if submodules_copy != submodules:
            shutil.copyfile(".gitmodules", ".gitmodules.old")
            with open(".gitmodules", "w") as f:
                f.write(submodules)
        for i in url_path:
            sub_cmd_copy = sub_cmd.copy()
            sub_cmd_copy.append(i)
            os.system(" ".join(sub_cmd_copy))
            logger. info(" ".join(sub_cmd_copy))
            logger. info("cd %s" % i)
            abs_path = os.path.abspath(".")
            try:
                os.chdir(i)
                recursion_sub(args)
            except:
                logger.error("path %s is not exists" % (i))
            os.chdir(abs_path)
        if url_path != []:
            logger.info(str(url_path))


def main():
    # 获取传入参数
    args = get_args()
    # 下载主库
    name = download_main(args)
    # 下载子模块
    if args.submodule:
        logger. info("cd %s" % name)
        os.chdir(name)
        recursion_sub(args)
    logger. info("download success")


if __name__ == "__main__":
    main()


# pyinstaller -F -i  favicon.ico .\gdt.py 打包脚本
